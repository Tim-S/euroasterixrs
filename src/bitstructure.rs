pub trait BitStore {
    fn bit(&self, i: usize) -> bool;
    fn bits(&self, i: usize, len: usize) -> u64;
}

impl BitStore for u8 {
    fn bit(&self, i: usize) -> bool {
        let bit = i%8;
        if self >> bit & 1 == 1 {true} else {false}
    }

    fn bits(&self, i: usize, len: usize) -> u64 {
        let bit = i%8;
        (self >> bit & mask(len) as u8) as u64
    }
}

impl BitStore for &[u8] {
    #[inline(always)]
    fn bit(&self, i: usize) -> bool{
        let octet = octet(self, i);
        self.get(octet).unwrap().bit(i)
    }
    #[inline(always)]
    fn bits(&self, i: usize, len: usize) -> u64 {
        let bit = i%8;
        if bit+len <= 8 {
            let octet = octet(self, i);
            self.get(octet).unwrap().bits(i, len)
        } else{
            let bits_rhs = 8-bit;
            self.bits(i, bits_rhs) | self.bits(i+bits_rhs, len-bits_rhs) << bits_rhs as u64
        }
    }
}

#[inline(always)]
fn octet(buffer: &[u8], bit: usize)->usize{
    buffer.len()-1-(bit/8)
}

#[inline(always)]
fn mask(len: usize) -> usize{
    (1<<len)-1
}

pub trait BitStoreMut {
    fn set_bit(&mut self, i: usize, value: bool);
    fn set_bits(&mut self, i: usize, len: usize, value: u64);
}

impl BitStoreMut for u8 {
    fn set_bit(&mut self, i: usize, value: bool) {
        if value { // set bit
            *self |= 1u8 << i as u8;
        } else { //unset bit
            *self &= 0xFFu8 ^ (1 << i as u8)
        }
    }

    fn set_bits(&mut self, i: usize, len: usize, value: u64) {
        *self &= !((mask(len)<<i) as u8); // set bits to 0
        *self |= (value << i as u64) as u8;
    }
}

impl BitStoreMut for &mut [u8] {
    #[inline(always)]
    fn set_bit(&mut self, i: usize, value: bool) {
        let octet = octet(self, i);
        self.get_mut(octet).unwrap().set_bit(i, value);
    }
    #[inline(always)]
    fn set_bits(&mut self, i: usize, len: usize, value: u64) {
        let bit = i%8;
        if bit+len <= 8 { // bits are in one octet:
            let octet = octet(self, i);
            self.get_mut(octet).unwrap().set_bits(bit, len, value);
        } else { // bits overlap multiple octets, so set bits in last octet and recur with the remaining bits of the value:
            let bits_rhs = 8-bit;
            self.set_bits(i, bits_rhs, value & mask(bits_rhs) as u64);
            self.set_bits(i+bits_rhs, len-bits_rhs, value>>bits_rhs as u64);
        }
    }
}
