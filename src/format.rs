use std::io::{Write, Error, Read};
use crate::bitstructure::{BitStore, BitStoreMut};
use std::ops::{Index, IndexMut};
use std::collections::BTreeMap;

#[cfg(test)]
mod tests {
    use crate::format::{ExtendedLength, FSpec, DataField, Compound, Repetitive};
    use std::io::{Write, Read};
    use crate::bitstructure::{BitStore, BitStoreMut};

    #[test]
    fn writing_extended_length_works() {
        let buf:&[u8] = &[0b1, 0, 0b1, 0, 0, 0][..];
        let mut f: ExtendedLength = ExtendedLength::new(1,2);
        assert_eq!(5, f.write(buf).unwrap());

        assert_eq!(1, f.primary.as_slice().bits(0, 8));
        assert_eq!(2, f.extents.len());
        assert_eq!(1, f.extents[0].as_slice().bits(0, 16));
        assert_eq!(0, f.extents[1].as_slice().bits(0, 16));

    }
    #[test]
    fn reading_extended_length_works() {
        let buf = &mut [0, 0, 0, 0, 0, 0, 0][..];

        let mut el: ExtendedLength = ExtendedLength::new(1,2);
        el.resize(3);
        assert_eq!(el.len(), 4);
        assert_eq!(7, el.read(buf).unwrap());
        assert_eq!(buf, [0b1, 0, 0b1, 0, 0b1, 0, 0]);

        el[1].as_mut_slice().set_bit(1, true);
        el[2].as_mut_slice().set_bit(0b10, true);
        assert_eq!(7, el.read(buf).unwrap());
        assert_eq!(buf, [0b1, 0, 0b11, 0, 0b101, 0, 0]);
    }
    #[test]
    fn check_enabled_frns_via_index_works() {
        let buf:&[u8] = &[0b11, 0b1000_0001, 0b0000_0100][..];
        let mut f = FSpec::new();
        assert_eq!(3, f.write(buf).unwrap());

        assert!(f[6]);
        assert!(f[7]);
        assert!(f[19]);

        assert!(!f[100]); // should return false for frns not even contained in FSPEC

        for i in 0..6{
            assert!(!f[i])
        }
        for i in 8..19{
            assert!(!f[i])
        }
        for i in 20..21 as u8{
            assert!(!f[i])
        }
    }

    #[test]
    fn compound_works() {
        const SF1_FRN: u8 = 6;
        const SF2_FRN: u8 = 7;
        const FSPEC_LEN: usize = 2 as usize;
        const SF1_LEN: usize = 1;
        const SF2_LEN: usize = 2;

        let buf:&[u8] = &[0b0000_0011, 0b1000_0000, 0b0000_0100, 0x2, 0x3][..];

        let mut c: Compound = Compound::default();
        c[SF1_FRN] = DataField::FixedLength(vec![0; SF1_LEN]);
        c[SF2_FRN] = DataField::FixedLength(vec![0; SF2_LEN]);

        assert_eq!(FSPEC_LEN + SF1_LEN + SF2_LEN, c.write(buf).unwrap());
        assert!(c.is_subfield_enabled(SF1_FRN));
        assert!(c.is_subfield_enabled(SF2_FRN));

        assert_eq!(match c.uap.get(&SF1_FRN).unwrap() {
            DataField::FixedLength(df) => {df.as_slice().bits(0, 8)},
            _ => 0,
        }, 0b100);

        assert_eq!(match &c[SF2_FRN]{
            DataField::FixedLength(df) => {df.as_slice().bits(0, 16)},
            _ => 0
        }, 0x0203)
    }

    #[test]
    fn writing_explicit_length_works(){
        let buf:&[u8] = &[3, 1, 2][..];
        let mut el = DataField::ExplicitLength(vec![]);
        assert_eq!(3, el.write(buf).unwrap());

        if let DataField::ExplicitLength(df)  =  el {
            assert_eq!(df.len(), 2);
            assert_eq!(df[0], 1);
            assert_eq!(df[1], 2);
        }
        else{
            panic!();
        }
    }

    #[test]
    fn writing_repetitive_works(){
        let buf:&[u8] = &[3, 1, 2, 3, 4, 5, 6][..];
        let mut r = DataField::Repetitive(Repetitive{
            len_each_repetition: 2,
            repetitions: vec![]
        });
        assert_eq!(7, r.write(buf).unwrap());

        if let DataField::Repetitive(df)  =  r {
            assert_eq!(df.repetitions.len(), 3);
            assert_eq!(df.repetitions[0].as_slice().bits(0, 16), 0x0102);
            assert_eq!(df.repetitions[1].as_slice().bits(0, 16), 0x0304);
            assert_eq!(df.repetitions[2].as_slice().bits(0, 16), 0x0506);
        }
        else{
            panic!();
        }
    }
    #[test]
    fn reading_compound_to_buffer_works(){
        const SF1_FRN: u8 = 6;
        const SF2_FRN: u8 = 7;
        const SF1_LEN: usize = 1;
        const SF2_LEN: usize = 2;

        let buf = &mut [0u8, 0, 0, 0, 0, 0, 0][..];

        let mut  sf1 = vec![0; SF1_LEN];
        sf1.as_mut_slice().set_bits(0, 8, 16);
        let mut sf2 = vec![0; SF2_LEN];
        sf2.as_mut_slice().set_bits(0, 16, 0x0203);

        let mut c: Compound = Compound::default();
        c[SF1_FRN] = DataField::FixedLength(sf1);
        c[SF2_FRN] = DataField::FixedLength(sf2);

        for &frn in &[SF1_FRN, SF2_FRN] {
            c.enable_subfield(frn, true);
        }
        assert_eq!(2, c.fspec.len());
        assert!(c.fspec[SF1_FRN]);
        assert!(c.fspec[SF2_FRN]);

        assert_eq!(5, c.read(buf).unwrap());
        assert_eq!(buf, [0b11, 0b1000_0000, 16, 2, 3, 0, 0]);
    }

    #[test]
    fn reading_data_field_into_buffer_works(){
        let buf = &mut vec![0u8; 6][..];
        assert_eq!(5, DataField::FixedLength(vec![1, 2, 3, 4, 5]).read(buf).unwrap());
        assert_eq!(buf, [1, 2, 3, 4, 5, 0]);

        let buf = &mut vec![0u8; 6][..];
        let mut el = ExtendedLength::new(1, 2);
        el.resize(2);
        el[0].as_mut_slice().set_bits(1, 7, 2);
        el[1].as_mut_slice().set_bits(1, 15, 3);
        el[2].as_mut_slice().set_bits(1, 15, 4);
        assert_eq!(5, DataField::ExtendedLength(el).read(buf).unwrap());
        assert_eq!(buf, [0b10_1, 0, 0b11_1, 0, 0b100_0, 0]);

        let buf = &mut vec![0u8; 6][..];
        assert_eq!(5, DataField::ExplicitLength(vec![1, 2, 3, 4]).read(buf).unwrap());
        assert_eq!(buf, [5, 1, 2, 3, 4, 0]);

        let buf = &mut vec![0u8; 6][..];
        let mut rep = Repetitive::new(2);
        rep.resize(2);
        rep[0].as_mut_slice().set_bits(0, 16, 1);
        rep[1].as_mut_slice().set_bits(0, 16, 3);
        assert_eq!(5, DataField::Repetitive(rep).read(buf).unwrap());
        assert_eq!(buf, [2, 0, 1, 0, 3, 0]);
    }
}

pub type FixedLength = Vec<u8>;

pub struct ExtendedLength{
    primary_len: u16,
    extent_len: u16,
    primary: FixedLength,
    extents: Vec<FixedLength>
}

impl ExtendedLength {
    pub fn new(primary_len: u16, extent_len: u16)->Self{
        let primary = vec![0; primary_len as usize];
        ExtendedLength{
            primary_len,
            extent_len,
            primary,
            extents: vec![]
        }
    }

    pub fn len(&self) -> usize{
        1 + self.extents.len()
    }

    pub fn resize(&mut self, index: u16){
        let extent_len = self.extent_len;
        let extents =&mut self.extents;
        let diff = index-extents.len() as u16;
        if diff>0 {
            const FX_BIT: usize = 0;
            extents.resize_with(index as usize, ||{vec![0; extent_len as usize]});
            let new_extent_count = extents.len();
            self.primary.as_mut_slice().set_bit(FX_BIT, true);
            for extent in extents.iter_mut().take(new_extent_count-1) {
                extent.as_mut_slice().set_bit(FX_BIT, true);
            }
        }
    }
}

impl Write for ExtendedLength {
    fn write(&mut self, buf: &[u8]) -> Result<usize, Error> {
        self.extents.clear();
        let mut len = self.primary_len as usize;
        let mut slice = &buf[0..len];
        self.primary = slice.to_vec();

        const FX_BIT: usize = 0;
        while slice.bit(FX_BIT){
            let pos = len;
            len += self.extent_len as usize;
            slice = &buf[pos..len];
            let extent:FixedLength = slice.to_vec();
            self.extents.push(extent)
        }
        Result::Ok(len as usize)
    }

    fn flush(&mut self) -> Result<(), Error> {
        Result::Ok(())
    }
}

impl Index<u16> for ExtendedLength{
    type Output = FixedLength;

    fn index(&self, index: u16) -> &Self::Output {
        if index == 0 {
            &self.primary
        } else{
            &self.extents[(index-1) as usize]
        }
    }
}

impl IndexMut<u16> for ExtendedLength{
    fn index_mut(&mut self, index: u16) -> &mut Self::Output {
        if index == 0 {
            &mut self.primary
        } else{
            &mut self.extents[(index-1) as usize]
        }
    }
}

impl Read for ExtendedLength {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error> {
        match self.primary.as_slice().read(buf) {
            Ok(mut bytes_read) => {
                for extent in &mut self.extents {
                    match extent.as_slice().read(&mut buf[bytes_read..]) {
                        Ok(extent_bytes_read) => {bytes_read+=extent_bytes_read},
                        Err(e) => {return Err(e)},
                    }
                }
                Result::Ok(bytes_read)
            },
            Err(e) => Result::Err(e),
        }
    }
}

pub struct FSpec{
    content: ExtendedLength,
}

impl FSpec {
    pub fn new()->Self{
        FSpec{
            content: ExtendedLength::new(1,1),
        }
    }
    pub fn len(&self)->usize{
        self.content.len()
    }

    pub fn enable(&mut self, frn: u8, enable: bool){
        let index = (frn/7) as u16;
        let bit = frn%7;
        self.content.resize(index);
        let extent = &mut self.content[index];
        if enable {
            extent[0] |= 0b1000_0000 >> bit;
        }else{
            extent[0] &= !(0b1000_0000 >> bit)
        }
    }
}

impl Write for FSpec {
    fn write(&mut self, buf: &[u8]) -> Result<usize, Error> {
        self.content.write(buf)
    }

    fn flush(&mut self) -> Result<(), Error> {
        self.content.flush()
    }
}

impl Read for FSpec {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error> {
        self.content.read(buf)
    }
}

impl Index<u8> for FSpec{
    type Output = bool;

    fn index(&self, index: u8) -> &Self::Output {
        let extent = index/7;
        if extent > 1+self.len() as u8 {
            return &false;
        }

        let bit_in_extent = index % 7;
        let extent_content = self.content[extent as u16][0];
        let is_set = (0b1000_0000 >> bit_in_extent & extent_content) > 0;
        if is_set { &true } else { &false }
    }
}

pub enum DataField{
    FixedLength(FixedLength),
    ExtendedLength(ExtendedLength),
    None,
    ExplicitLength(ExplicitLength),
    Repetitive(Repetitive),
    Compound(Compound)
}

impl Write for DataField {
    fn write(&mut self, buf: &[u8]) -> Result<usize, Error> {
        match self {
            DataField::FixedLength(df) => df.write(&buf[0..df.len()]),
            DataField::ExtendedLength(df) => df.write(buf),
            DataField::None => Result::Ok(0),
            DataField::ExplicitLength(df) =>
                match df.write(&buf[1..buf[0] as usize]){
                    Ok(content_bytes) => Result::Ok(1+content_bytes),
                    Err(e) => Result::Err(e),
                },
            DataField::Repetitive(df) => df.write(buf),
            DataField::Compound(df) => df.write(buf),
        }
    }

    fn flush(&mut self) -> Result<(), Error> {
        match self {
            DataField::FixedLength(df) => df.flush(),
            DataField::ExtendedLength(df) => df.flush(),
            DataField::None => Result::Ok(()),
            DataField::ExplicitLength(df) => df.flush(),
            DataField::Repetitive(df) => df.flush(),
            DataField::Compound(df) => df.flush(),
        }
    }
}

impl Read for DataField{
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error> {
        match self {
            DataField::FixedLength(df) => df.as_slice().read(buf),
            DataField::ExtendedLength(df) => df.read(buf),
            DataField::None => Result::Ok(0),
            DataField::ExplicitLength(df) => {
                match df.as_slice().read(&mut buf[1..]) {
                    Ok(content_bytes_read) => {
                        let bytes_read = 1 + content_bytes_read;
                        buf[0] = bytes_read as u8;
                        Result::Ok(bytes_read)
                    },
                    Err(e) => Err(e),
                }
            },
            DataField::Repetitive(df) => df.read(buf),
            DataField::Compound(df) => df.read(buf)
        }
    }
}

pub type UAP =  BTreeMap<u8, DataField>;

pub struct Compound{
    fspec: FSpec,
    uap: UAP
}

impl Compound {
    pub fn new() -> Self{
        Compound{
            fspec: FSpec::new(),
            uap: UAP::new()
        }
    }
    pub fn enable_subfield(&mut self, index: u8, value: bool){
        self.fspec.enable(index, value)
    }

    pub fn is_subfield_enabled(&self, index: u8)->bool{
        self.fspec[index]
    }
}

impl Write for Compound {
    fn write(&mut self, buf: &[u8]) -> Result<usize, Error> {
        let uap = &mut self.uap;
        match self.fspec.write(buf) {
            Ok(mut bytes_written) => {
                for (&frn, data_field) in uap.iter_mut(){
                    if self.fspec[frn] {
                        match data_field.write(&buf[bytes_written..]) {
                            Ok(data_field_bytes_written) =>
                                {bytes_written+=data_field_bytes_written},
                            Err(e) => return Result::Err(e),
                        }
                    }
                }
                Ok(bytes_written)
            },
            Err(e) => Err(e),
        }
    }

    fn flush(&mut self) -> Result<(), Error> {
        Result::Ok(())
    }
}

impl Read for Compound{
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error> {
        let uap = &mut self.uap;
        match self.fspec.read(buf) {
            Ok(mut bytes_read) => {
                for (&frn, data_field) in uap.iter_mut() {
                    if self.fspec[frn] {
                        match data_field.read(&mut buf[bytes_read..]) {
                            Ok(data_field_bytes_read) =>
                                { bytes_read += data_field_bytes_read },
                            Err(e) => return Result::Err(e),
                        }
                    }
                }
                Ok(bytes_read)
            },
            Err(e) => Err(e),
        }
    }
}

impl Index<u8> for Compound{
    type Output = DataField;

    fn index(&self, index: u8) -> &Self::Output {
        if self.fspec[index] {
            match self.uap.get(&index) {
                None => &DataField::None,
                Some(df) => df,
            }
        }
        else { &DataField::None }
    }
}

impl IndexMut<u8> for Compound{
    fn index_mut(&mut self, index: u8) -> &mut Self::Output {
        if !self.uap.contains_key(&index){
            self.uap.insert(index, DataField::None);
        }
        self.uap.get_mut(&index).unwrap()
    }
}

impl Default for Compound{
    fn default() -> Self {
        Compound::new()
    }
}

pub type ExplicitLength = Vec<u8>;
pub struct Repetitive{
    len_each_repetition: u16,
    repetitions: Vec<FixedLength>
}

impl Repetitive {
    pub fn new(len_each_repetition: u16)->Self{
        Repetitive {
            len_each_repetition,
            repetitions: vec![]
        }
    }

    pub fn len(&self) -> u8{
        self.repetitions.len() as u8
    }

    pub fn resize(&mut self, repetitions: u8){
        self.repetitions.resize(repetitions as usize, vec![0u8; self.len_each_repetition as usize]);
    }
}

impl Write for Repetitive {
    fn write(&mut self, buf: &[u8]) -> Result<usize, Error> {
        let repetitions = buf[0] as usize;
        let len_each_repetition = self.len_each_repetition;
        let reps = &mut self.repetitions;
        reps.resize_with(repetitions as usize, || { let mut new=FixedLength::new(); new.resize(len_each_repetition as usize, 0); new });
        let mut bytes_written = 1;
        for i in 0..repetitions {
            bytes_written += reps[i].write(&buf[bytes_written..bytes_written+len_each_repetition as usize]).unwrap();
        }
        Result::Ok(bytes_written)
    }

    fn flush(&mut self) -> Result<(), Error> {
        Result::Ok(())
    }
}

impl Read for Repetitive {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error> {
        buf[0] = self.repetitions.len() as u8;
        let mut bytes_read : usize = 1;
        for repetition in &self.repetitions {
            match repetition.as_slice().read(&mut buf[bytes_read..]) {
                Ok(repetition_bytes) => {bytes_read+=repetition_bytes},
                Err(e) => return Err(e),
            }
        }
        Ok(bytes_read)
    }
}

impl Index<u8> for Repetitive{
    type Output = FixedLength;

    fn index(&self, index: u8) -> &Self::Output {
        &self.repetitions[index as usize]
    }
}

impl IndexMut<u8> for Repetitive{
    fn index_mut(&mut self, index: u8) -> &mut Self::Output {
        &mut self.repetitions[index as usize]
    }
}